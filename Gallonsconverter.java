public class Gallonsconverter {

    public static void main(String[] args) {

        //Verbessern Sie das Programm GallonsConverter.java. Wir wollen nun eine Tabelle
        //von Umrechnunge ausgeben, beginnend mit 1 Gallone bis 100. Nach jeweils 10
        //Gallonen soll eine leere Zeile ausgegeben werden. Verwenden Sie dazu eine
        //Variable, die die Anzahl Zeilen zählt.
        //Überlegen Sie sich, welche Form von Iteration Sie verwenden können



        //Define Variables
        double litres;
        int counter = 10;

        //Iteration : Gallons bis 100 zählen und ausgeben wie viel es in Liter ist.
        for (int gallons = 1; gallons <= 100; gallons++) {
            litres = gallons / 3.7854;
            System.out.println(gallons + " gallon is " + litres + " litres. ");

            //Verzweigung : Falls Gallons aufgeht mit 10, Abstand machen
            if (gallons % counter == 0) {
                System.out.println("\n");
            }

        }

    }

}
