public class Temperatur {

    public static void main(String[] args) {
        //Celsius – Fahrenheit Berechner
        //Schreiben Sie ein Programm, das dem Benutzer erlaubt, entweder die Grad in
        //Celcius oder Fahrenheit zu berechnen. Überlegen Sie sich zuerst, welche
        //Datentypen Sie verwenden sollten.
        //Geben Sie eine Meldung aus, wenn die Temperatur unter 0℃ (Gefrierpunkt), oder 100℃ (Siedepunkt) ist!
        /*
        0 °C	32.0 °F
        1 °C	33.8 °F
        */
        System.out.println("Celcius oder Fahrenheit");
        double temp = Input.inputDouble("0 für Celcius, 1 für Fahrenheit:");

        //Verzweigung
        if (temp == 0) {
            double celcius = Input.inputDouble("Geben Sie eine Zahl für Celcius ein: ");
            double F;
            F = celcius * 1.8 + 32;
            System.out.println(F);
        } else {
            double fahrenheit = Input.inputDouble("Geben Sie eine Zahl für Fahrenheit ein: ");
            double C;
            C = (fahrenheit - 32) / 1.8;
            System.out.println(C);
            if (C == 0) {
                System.out.println("Gefrierpunkt erreicht!");
            } else if (C == 100) {
                System.out.println("Siedepunkt erreicht!");
            }
        }


    }
}
